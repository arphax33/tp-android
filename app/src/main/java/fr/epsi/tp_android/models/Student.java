package fr.epsi.tp_android.models;

import org.json.JSONObject;

import java.io.Serializable;

public class Student implements Serializable {
    private String name;
    private String forname;
    private String email;
    private String group;
    private String pictureUrl;

    public Student(String name, String forname, String email, String group, String pictureUrl){
        this.name = name;
        this.forname = forname;
        this.email = email;
        this.group = group;
        this.pictureUrl = pictureUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getForname() {
        return forname;
    }

    public void setForname(String forname) {
        this.forname = forname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}

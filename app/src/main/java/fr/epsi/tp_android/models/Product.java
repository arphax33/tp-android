package fr.epsi.tp_android.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

public class Product implements Serializable {
    private String name;
    private String description;
    private String pictureUrl;

    public Product(JSONObject json) {
        name = json.optString("name");
        description = json.optString("description");
        pictureUrl = json.optString("picture_url");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}

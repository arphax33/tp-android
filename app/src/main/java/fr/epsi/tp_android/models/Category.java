package fr.epsi.tp_android.models;

import org.json.JSONObject;

import java.io.Serializable;

public class Category implements Serializable {
    private int id;
    private String title;
    private String productUrl;

    public Category(JSONObject json) {
        id = json.optInt("category_id");
        title = json.optString("title");
        productUrl = json.optString("product_url");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }
}

package fr.epsi.tp_android;

import android.app.Application;

public class TPAndroidApp extends Application {
    private String title;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

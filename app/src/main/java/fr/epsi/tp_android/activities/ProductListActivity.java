package fr.epsi.tp_android.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import fr.epsi.tp_android.R;
import fr.epsi.tp_android.adapters.CategoryAdapter;
import fr.epsi.tp_android.adapters.ProductAdapter;
import fr.epsi.tp_android.models.Category;
import fr.epsi.tp_android.models.Product;
import fr.epsi.tp_android.services.HttpService;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.Normalizer;
import java.util.ArrayList;

public class ProductListActivity extends TPAndroidActivity {
    private Category category;
    private ArrayList<Product> products;
    private ProductAdapter adapter;

    public static void display(TPAndroidActivity activity, Category category){
        Intent intent = new Intent(activity, ProductListActivity.class);
        intent.putExtra("category", category);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        this.category = (Category) this.getIntent().getSerializableExtra("category");
        showBackBtn();
        setTitle(category.getTitle());

        products = new ArrayList<Product>();
        adapter = new ProductAdapter(this, R.layout.content_product, products);
        String normalizedName = StringUtils.stripAccents(category.getTitle()).toLowerCase();
        System.out.println(normalizedName);
        String urlString = "https://djemam.com/epsi/" + normalizedName + ".json";
        ListView productList = findViewById(R.id.productList);

        productList.setAdapter(adapter);
        productList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProductDetailActivity.display(ProductListActivity.this, products.get(position));
            }
        });

        new HttpService(urlString, new HttpService.TaskListener() {
            @Override
            public void webServiceDone(String result) {
                initData(result);
            }

            @Override
            public void webServiceError(Exception e) {
                System.out.println(e.getMessage()); //TODO: display Toast
            }
        }).execute();
    }

    private void initData(String data) {
        try {
            JSONObject jsonObject;
            jsonObject = new JSONObject(data);
            JSONArray jsonArray = jsonObject.getJSONArray("items");
            for(int i = 0; i<jsonArray.length(); i++){
                Product product = new Product(jsonArray.getJSONObject(i));
                products.add(product);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        adapter.notifyDataSetChanged();
    }
}

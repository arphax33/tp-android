package fr.epsi.tp_android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import fr.epsi.tp_android.R;
import fr.epsi.tp_android.models.Student;

public class StudentActivity extends TPAndroidActivity {
    private Student student;

    public static void display(TPAndroidActivity activity, Student student){
        Intent intent = new Intent(activity, StudentActivity.class);
        intent.putExtra("student", student);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);
        student = (Student) getIntent().getExtras().get("student");
        showBackBtn();
        setTitle(student.getName());

        ImageView picture = findViewById(R.id.student_picture);
        TextView name = findViewById(R.id.name);
        TextView mail = findViewById(R.id.mail);
        TextView group = findViewById(R.id.group);
        TextView link = findViewById(R.id.link);
        String completeName = student.getName() + " " + student.getForname();

        Picasso.get().load(student.getPictureUrl()).into(picture);
        name.setText(completeName);
        mail.setText(student.getEmail());
        group.setText(student.getGroup());

        link.setMovementMethod(LinkMovementMethod.getInstance());
    }
}

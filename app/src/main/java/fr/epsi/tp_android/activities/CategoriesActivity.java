package fr.epsi.tp_android.activities;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.os.Bundle;
import fr.epsi.tp_android.R;
import fr.epsi.tp_android.adapters.CategoryAdapter;
import fr.epsi.tp_android.models.Category;
import fr.epsi.tp_android.services.HttpService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CategoriesActivity extends TPAndroidActivity {
    private ArrayList<Category> categories;
    private CategoryAdapter adapter;

    public static void display(TPAndroidActivity activity){
        Intent intent = new Intent(activity, CategoriesActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        showBackBtn();
        setTitle(getString(R.string.categories_title));

        categories = new ArrayList<Category>();
        adapter = new CategoryAdapter(this, R.layout.content_category, categories);
        String urlString = "https://djemam.com/epsi/categories.json";
        ListView categoryList = findViewById(R.id.categoryList);

        categoryList.setAdapter(adapter);
        categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProductListActivity.display(CategoriesActivity.this, categories.get(position));
            }
        });

        new HttpService(urlString, new HttpService.TaskListener() {
            @Override
            public void webServiceDone(String result) {
                initData(result);
            }

            @Override
            public void webServiceError(Exception e) {
                System.out.println(e.getMessage()); //TODO: display Toast
            }
        }).execute();
    }

    private void initData(String data) {
        try {
            JSONObject jsonObject;
            jsonObject = new JSONObject(data);
            JSONArray jsonArray = jsonObject.getJSONArray("items");
            for(int i = 0; i<jsonArray.length(); i++){
                Category category = new Category(jsonArray.getJSONObject(i));
                categories.add(category);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        adapter.notifyDataSetChanged();
    }
}

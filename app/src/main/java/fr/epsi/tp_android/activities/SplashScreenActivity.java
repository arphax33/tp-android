package fr.epsi.tp_android.activities;

import android.content.Intent;
import android.os.Handler;
import android.view.Window;
import android.os.Bundle;
import fr.epsi.tp_android.R;

public class SplashScreenActivity extends TPAndroidActivity {
    private static int SPLASH_SCREEN_TIMEOUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashScreenActivity.this, HomeActivity.class);
                startActivity(i);
                finish();
            }
        }, SPLASH_SCREEN_TIMEOUT);
    }
}

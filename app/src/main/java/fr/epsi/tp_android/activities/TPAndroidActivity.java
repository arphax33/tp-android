package fr.epsi.tp_android.activities;

import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import fr.epsi.tp_android.R;
import fr.epsi.tp_android.TPAndroidApp;

public class TPAndroidActivity extends AppCompatActivity implements View.OnClickListener {

    protected TPAndroidApp tpAndroidActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        tpAndroidActivity = (TPAndroidApp) getApplication();
    }

    protected void showBackBtn() {
        ImageView imageView = findViewById(R.id.back_btn);
        if(imageView != null) {
            imageView.setVisibility(View.VISIBLE);
            imageView.setOnClickListener(this);
        }
    }

    protected void setTitle(String title) {
        TextView textViewTitle = findViewById(R.id.activity_title);
        if(textViewTitle != null) {
            textViewTitle.setText(title);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back_btn:
                finish();
                break;
        }
    }
}

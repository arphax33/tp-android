package fr.epsi.tp_android.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import fr.epsi.tp_android.R;
import fr.epsi.tp_android.models.Product;

public class ProductDetailActivity extends TPAndroidActivity {
    private Product product;

    public static void display(TPAndroidActivity activity, Product product){
        Intent intent = new Intent(activity, ProductDetailActivity.class);
        intent.putExtra("product", product);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        this.product = (Product) this.getIntent().getSerializableExtra("product");
        showBackBtn();
        setTitle(product.getName());

        ImageView productDetailImage = findViewById(R.id.product_detail_image);
        TextView productDetailDesc = findViewById(R.id.product_detail_description);

        Picasso.get().load(product.getPictureUrl()).into(productDetailImage);
        productDetailDesc.setText(product.getDescription());
    }
}

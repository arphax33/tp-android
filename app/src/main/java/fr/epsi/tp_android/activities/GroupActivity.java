package fr.epsi.tp_android.activities;

import android.content.Intent;
import android.view.View;
import android.os.Bundle;
import fr.epsi.tp_android.R;
import fr.epsi.tp_android.models.Student;

import java.util.ArrayList;

public class GroupActivity extends TPAndroidActivity implements View.OnClickListener {
    private ArrayList<Student> students = new ArrayList<>();

    public static void display(TPAndroidActivity activity){
        Intent intent=new Intent(activity, GroupActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);
        setTitle(getString(R.string.group_title));
        showBackBtn();
        initData();

        findViewById(R.id.student_1).setOnClickListener(this);
        findViewById(R.id.student_2).setOnClickListener(this);
        findViewById(R.id.student_3).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back_btn:
                finish();
                break;
            case R.id.student_1:
                StudentActivity.display(GroupActivity.this, students.get(0));
                break;
            case R.id.student_2:
                StudentActivity.display(GroupActivity.this, students.get(1));
                break;
            case R.id.student_3:
                StudentActivity.display(GroupActivity.this, students.get(2));
                break;
        }
    }

    private void initData() {
        Student student1 = new Student("Arraud", "Allan", "allan.arraud@epsi.fr" ,"GRP2" ,
                "https://www.numerama.com/content/uploads/2019/05/trou-noir-espace-univers-astronomie.jpg");
        Student student2 = new Student("Benaissa", "Hajar", "hajar.benaissa@epsi.fr" ,"GRP2" ,
                "https://media.gettyimages.com/photos/colorful-powder-explosion-in-all-directions-in-a-nice-composition-picture-id890147976?s=2048x2048");
        Student student3 = new Student("Ba", "Moussa", "moussa.ba2@epsi.fr" ,"GRP2" ,
                "https://helpx.adobe.com/content/dam/help/en/stock/how-to/visual-reverse-image-search/jcr_content/main-pars/image/visual-reverse-image-search-v2_intro.jpg");
        students.add(student1);
        students.add(student2);
        students.add(student3);
    }
}

package fr.epsi.tp_android.activities;

import android.view.View;
import android.os.Bundle;
import fr.epsi.tp_android.R;

public class HomeActivity extends TPAndroidActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.area_1).setOnClickListener(this);
        findViewById(R.id.area_2).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.area_1:
                GroupActivity.display(HomeActivity.this);
                break;
            case R.id.area_2:
                CategoriesActivity.display(HomeActivity.this);
                break;
        }
    }
}

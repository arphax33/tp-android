package fr.epsi.tp_android.services;

import android.os.AsyncTask;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpService extends AsyncTask<Void, Void, Object> {
    public interface TaskListener {
        void webServiceDone(String result);
        void webServiceError(Exception e);
    }

    private TaskListener taskListener;
    private String urlString;

    public HttpService(String urlString, TaskListener listener){
        taskListener = listener;
        this.urlString = urlString;
    }

    @Override
    protected Object doInBackground(Void... voids) {
        return call(urlString);
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if(o instanceof Exception){
            taskListener.webServiceError((Exception)o);
        }
        else
            taskListener.webServiceDone(o.toString());
    }


    public Object call(String urlStr) {
        try {
            URL url = null;
            url = new URL(urlStr);
            HttpURLConnection urlConnection;
            if(urlStr.startsWith("https:")){
                urlConnection = (HttpsURLConnection) url.openConnection();
            }
            else{
                urlConnection = (HttpURLConnection) url.openConnection();
            }
            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                return convertStreamToString(in);
            } finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return e;
        } catch (IOException e) {
            e.printStackTrace();
            return e;
        }
    }

    private String convertStreamToString(InputStream is){
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

            StringBuffer stringBuffer = new StringBuffer("");
            String line;

            String NL = System.getProperty("line.separator");
            while ((line = bufferedReader.readLine()) != null)
            {
                stringBuffer.append(line + NL);
            }
            bufferedReader.close();
            return stringBuffer.toString();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
